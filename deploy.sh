#!/bin/bash


node_app=`docker ps -a | grep nginx | awk '{print $NF}'`
if [ $node_app=='nginx' ]; then
    echo "nodeapp is running, lets delete"
        docker rm -f nginx
fi

images=`docker images | grep docker.io/nginx | awk '{print $3}'`
docker rmi $images
docker run -d -p 8080:8080 --name nginx $1
